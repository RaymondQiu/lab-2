/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author rqiu
 *
 */
public class Celsius extends Temperature{
  public Celsius(float t)
  {
	  super(t);
  }
  public String toString()
  {
	  return "" + value + " C";
  }
	public Temperature toCelsius() {
		return new Celsius(value);
	}
	public Temperature toFahrenheit() {
		return new Fahrenheit(value*9/5+32);
	}
	
	public Temperature toKelvin() {
		return new Kelvin((float)(value+273.15));
	}
}
