package edu.ucsd.cs110w.temperature;

/**
 * TODO (rqiu):write class javadoc 
 *
 * @author rqiu
 *
 */
public class Kelvin extends Temperature{
    public Kelvin(float t)
    {
    	super(t);
    }
    
    public String toString(){
    	return ""+value+" K";
    }

	public Temperature toCelsius() {
		return new Celsius((float)(value-273.15));
	}

	public Temperature toFahrenheit() {
		return new Fahrenheit((float)(value-273.15)*9/5+32);
	}

	public Temperature toKelvin() {
		return new Kelvin(value);
	}
	
}
