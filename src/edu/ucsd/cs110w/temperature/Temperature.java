/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author rqiu
 *
 */
public abstract class Temperature {
	protected float value;
	public Temperature(float v)
	{
	  value = v;
	}
	
	public final float getValue()
	{
		return value;
	}
	public abstract Temperature toCelsius();
	public abstract Temperature toFahrenheit();
	public abstract Temperature toKelvin();
}
