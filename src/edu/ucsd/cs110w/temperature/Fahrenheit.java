/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author rqiu
 *
 */
public class Fahrenheit extends Temperature {
  public Fahrenheit(float t)
  {
	  super(t);
  }
  public String toString()
  {
	  return "" + value + " F";
  }
	public Temperature toCelsius() {
		return new Celsius((value-32)*5/9);
	}
	public Temperature toFahrenheit() {
		return new Fahrenheit(value);
    }

	public Temperature toKelvin() {
		return new Kelvin((float)((value-32)*5/9+273.15));
	}
}
